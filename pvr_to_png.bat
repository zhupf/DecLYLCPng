@echo off
set path=%path%;"D:\Program Files\CodeAndWeb\TexturePacker\bin"
for /f "usebackq tokens=*" %%d in (`dir /s /b *.pvr.ccz`) do ( 
TexturePacker.exe --sheet "%%~dpnd.png" "%%d" --algorithm Basic --allow-free-size --no-trim --content-protection 9D01918EBA7ADFF4CC94CFF3046D1B6A
)
del/s/q/a *.pvr.ccz
del/s/q/a out.plist
pause